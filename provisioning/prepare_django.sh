#!/bin/bash

# Stop if an error is encountered.
set -e

python manage.py migrate
python manage.py sync_page_translation_fields
python manage.py update_translation_fields
python manage.py loaddata gestion sites articles
python manage.py loaddevdata
python manage.py syncmails gestioncof/management/data/custommail.json
