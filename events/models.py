from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Event(models.Model):
    title = models.CharField(_("titre"), max_length=200)
    location = models.CharField(_("lieu"), max_length=200)
    start_date = models.DateTimeField(_("date de début"), blank=True, null=True)
    end_date = models.DateTimeField(_("date de fin"), blank=True, null=True)
    description = models.TextField(_("description"), blank=True)
    image = models.ImageField(
        _("image"), blank=True, null=True, upload_to="imgs/events/"
    )
    registration_open = models.BooleanField(_("inscriptions ouvertes"), default=True)
    old = models.BooleanField(_("archiver (événement fini)"), default=False)
    subscribers = models.ManyToManyField(User, verbose_name=_("inscrit⋅e⋅s"))

    class Meta:
        verbose_name = _("événement")
        verbose_name_plural = _("événements")

    def __str__(self):
        return self.title


# TODO: gérer les options (EventOption & EventOptionChoice de gestioncof)
#       par exemple: "option végé au Mega (oui / non)"

# TODO: gérer les champs commentaires (EventCommentField & EventCommentChoice)
#       par exemple: "champ "allergies / régime particulier" au Mega
