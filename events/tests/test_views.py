from unittest import mock

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import Client, TestCase
from django.urls import reverse

from events.models import Event

User = get_user_model()


def make_user(name):
    return User.objects.create_user(username=name, password=name)


def make_staff_user(name):
    view_event_perm = Permission.objects.get(
        codename="view_event", content_type__app_label="events",
    )
    user = make_user(name)
    user.user_permissions.add(view_event_perm)
    return user


class CSVExportTest(TestCase):
    def setUp(self):
        # Signals handlers on login/logout send messages.
        # Due to the way the Django' test Client performs login, this raise an
        # error. As workaround, we mock the Django' messages module.
        patcher_messages = mock.patch("gestioncof.signals.messages")
        patcher_messages.start()
        self.addCleanup(patcher_messages.stop)

        self.staff = make_staff_user("staff")
        self.u1 = make_user("toto")
        self.u2 = make_user("titi")
        self.event = Event.objects.create(title="test_event", location="somewhere")
        self.event.subscribers.set([self.u1, self.u2])
        self.url = reverse("events:csv-participants", args=[self.event.id])

    def test_get(self):
        client = Client()
        client.force_login(self.staff)
        r = client.get(self.url)
        self.assertEqual(r.status_code, 200)

    def test_anonymous(self):
        client = Client()
        r = client.get(self.url)
        self.assertRedirects(
            r, "/login?next={}".format(self.url), fetch_redirect_response=False
        )

    def test_unauthorised(self):
        client = Client()
        client.force_login(self.u1)
        r = client.get(self.url)
        self.assertEqual(r.status_code, 403)
