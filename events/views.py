import csv

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.text import slugify

from events.models import Event


@login_required
@permission_required("events.view_event", raise_exception=True)
def participants_csv(request, event_id):
    event = get_object_or_404(Event, id=event_id)

    filename = "{}-participants.csv".format(slugify(event.title))
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(filename)

    writer = csv.writer(response)
    writer.writerow(["username", "email", "prénom", "nom de famille"])
    for user in event.subscribers.all():
        writer.writerow([user.username, user.email, user.first_name, user.last_name])

    return response
