from functools import reduce


def choices_length(choices):
    return reduce(lambda m, choice: max(m, len(choice[0])), choices, 0)
