from shared.tests.testcases import ViewTestCaseMixin as BaseViewTestCaseMixin

from .utils import create_member, create_staff, create_user


class ViewTestCaseMixin(BaseViewTestCaseMixin):
    """
    TestCase extension to ease testing of cof views.

    Most information can be found in the base parent class doc.
    This class performs some changes to users management, detailed below.

    During setup, three users are created:
    - 'user': a basic user without any permission,
    - 'member': (profile.is_cof is True),
    - 'staff': (profile.is_cof is True) && (profile.is_buro is True).
    """

    def get_users_base(self):
        return {
            "user": create_user("user"),
            "member": create_member("member"),
            "staff": create_staff("staff"),
        }
