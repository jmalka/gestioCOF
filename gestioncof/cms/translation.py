from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions

from .models import (
    COFActuIndexPage,
    COFActuPage,
    COFDirectoryEntryPage,
    COFDirectoryPage,
    COFPage,
    COFRootPage,
)


@register(COFRootPage)
class COFRootPageTr(TranslationOptions):
    fields = ("introduction",)


@register(COFPage)
class COFPageTr(TranslationOptions):
    fields = ("body",)


@register(COFActuIndexPage)
class COFActuIndexPageTr(TranslationOptions):
    fields = ()


@register(COFActuPage)
class COFActuPageTr(TranslationOptions):
    fields = ("chapo", "body")


@register(COFDirectoryPage)
class COFDirectoryPageTr(TranslationOptions):
    fields = ("introduction",)


@register(COFDirectoryEntryPage)
class COFDirectoryEntryPageTr(TranslationOptions):
    fields = ("body", "links")
