from django.shortcuts import render


def raw_calendar_view(request, year, month):
    return render(request, "cofcms/calendar_raw.html", {"month": month, "year": year})
