$(function() {
    $(".facteur").on("click", function(){
        var $this = $(this);
        var sticker = $this.attr('data-mref')
                           .replace('pont', '.')
                           .replace('arbre', '@')
                           .replace(/(.)-/g, '$1');
        
        var boite = $("<a>", {href:"ma"+"il"+"to:"+sticker}).text(sticker);
        $(this).before(boite)
               .remove();
    })
});
