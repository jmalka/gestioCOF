from django.urls import path

from gestioncof import views

export_patterns = [
    path("members", views.export_members, name="cof.membres_export"),
    path(
        "mega/avecremarques",
        views.export_mega_remarksonly,
        name="cof.mega_export_remarks",
    ),
    path(
        "mega/participants",
        views.export_mega_participants,
        name="cof.mega_export_participants",
    ),
    path("mega/orgas", views.export_mega_orgas, name="cof.mega_export_orgas"),
    path("mega", views.export_mega, name="cof.mega_export"),
]

surveys_patterns = [
    path("<int:survey_id>/status", views.survey_status, name="survey.details.status"),
    path("<int:survey_id>", views.survey, name="survey.details"),
]

events_patterns = [
    path("<int:event_id>", views.event, name="event.details"),
    path("<int:event_id>/status", views.event_status, name="event.details.status"),
]

calendar_patterns = [
    path("subscription", views.calendar, name="calendar"),
    path("<slug:token>/calendar.ics", views.calendar_ics, name="calendar.ics"),
]

clubs_patterns = [
    path("membres/<slug:name>", views.membres_club, name="membres-club"),
    path("liste", views.liste_clubs, name="liste-clubs"),
    path(
        "change_respo/<slug:club_name>/<int:user_id>",
        views.change_respo,
        name="change-respo",
    ),
]
