from dal import autocomplete
from django.db.models import Q


class Select2QuerySetView(autocomplete.Select2QuerySetView):
    model = None
    search_fields = []

    def get_queryset_filter(self):
        q = self.q
        filter_q = Q()

        if not q:
            return filter_q

        words = q.split()

        for word in words:
            for field in self.search_fields:
                filter_q |= Q(**{"{}__icontains".format(field): word})

        return filter_q

    def get_queryset(self):
        return self.model.objects.filter(self.get_queryset_filter())
