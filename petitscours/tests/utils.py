import os

from django.conf import settings
from django.core.management import call_command

from petitscours.models import (
    PetitCoursAbility,
    PetitCoursAttributionCounter,
    PetitCoursDemande,
    PetitCoursSubject,
)


def create_petitcours_ability(**kwargs):
    if "user" not in kwargs:
        kwargs["user"] = create_user()
    if "matiere" not in kwargs:
        kwargs["matiere"] = create_petitcours_subject()
    if "niveau" not in kwargs:
        kwargs["niveau"] = "college"
    ability = PetitCoursAbility.objects.create(**kwargs)
    PetitCoursAttributionCounter.get_uptodate(ability.user, ability.matiere)
    return ability


def create_petitcours_demande(**kwargs):
    return PetitCoursDemande.objects.create(**kwargs)


def create_petitcours_subject(**kwargs):
    return PetitCoursSubject.objects.create(**kwargs)


class PetitCoursTestHelpers:
    def require_custommails(self):
        data_file = os.path.join(
            settings.BASE_DIR, "gestioncof", "management", "data", "custommail.json"
        )
        call_command("syncmails", data_file, verbosity=0)
