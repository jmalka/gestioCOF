# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def swap_double_choice(apps, schema_editor):
    choices = apps.get_model("bda", "ChoixSpectacle").objects

    choices.filter(double_choice="double").update(double_choice="tmp")
    choices.filter(double_choice="autoquit").update(double_choice="double")
    choices.filter(double_choice="tmp").update(double_choice="autoquit")


class Migration(migrations.Migration):

    dependencies = [("bda", "0011_tirage_appear_catalogue")]

    operations = [
        # Temporarily allow an extra "tmp" value for the `double_choice` field
        migrations.AlterField(
            model_name="choixspectacle",
            name="double_choice",
            field=models.CharField(
                verbose_name="Nombre de places",
                max_length=10,
                default="1",
                choices=[
                    ("tmp", "tmp"),
                    ("1", "1 place"),
                    ("double", "2 places si possible, 1 sinon"),
                    ("autoquit", "2 places sinon rien"),
                ],
            ),
        ),
        migrations.RunPython(swap_double_choice, migrations.RunPython.noop),
        migrations.AlterField(
            model_name="choixspectacle",
            name="double_choice",
            field=models.CharField(
                verbose_name="Nombre de places",
                max_length=10,
                default="1",
                choices=[
                    ("1", "1 place"),
                    ("double", "2 places si possible, 1 sinon"),
                    ("autoquit", "2 places sinon rien"),
                ],
            ),
        ),
    ]
