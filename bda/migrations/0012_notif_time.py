# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("bda", "0011_tirage_appear_catalogue")]

    operations = [
        migrations.RenameField(
            model_name="spectaclerevente",
            old_name="answered_mail",
            new_name="confirmed_entry",
        ),
        migrations.AlterField(
            model_name="spectaclerevente",
            name="confirmed_entry",
            field=models.ManyToManyField(
                blank=True, related_name="entered", to="bda.Participant"
            ),
        ),
        migrations.AddField(
            model_name="spectaclerevente",
            name="notif_time",
            field=models.DateTimeField(
                blank=True, verbose_name="Moment d'envoi de la notification", null=True
            ),
        ),
    ]
