"""
Fichier principal de configuration des urls du projet GestioCOF
"""

from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as django_auth_views
from django.urls import include, path
from django.views.decorators.cache import cache_page
from django.views.generic.base import TemplateView
from django_cas_ng import views as django_cas_views
from django_js_reverse.views import urls_js
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from gestioncof import csv_views, views as gestioncof_views
from gestioncof.autocomplete import autocomplete
from gestioncof.urls import (
    calendar_patterns,
    clubs_patterns,
    events_patterns,
    export_patterns,
    surveys_patterns,
)

admin.autodiscover()

urlpatterns = [
    # Page d'accueil
    path("", gestioncof_views.HomeView.as_view(), name="home"),
    # Le BdA
    path("bda/", include("bda.urls")),
    # Les exports
    path("export/", include(export_patterns)),
    # Les petits cours
    path("petitcours/", include("petitscours.urls")),
    # Les sondages
    path("survey/", include(surveys_patterns)),
    # Evenements
    path("event/", include(events_patterns)),
    # Calendrier
    path("calendar/", include(calendar_patterns)),
    # Clubs
    path("clubs/", include(clubs_patterns)),
    # Authentification
    path(
        "cof/denied",
        TemplateView.as_view(template_name="cof-denied.html"),
        name="cof-denied",
    ),
    path("cas/login", django_cas_views.LoginView.as_view(), name="cas_login_view"),
    path("cas/logout", django_cas_views.LogoutView.as_view()),
    path(
        "outsider/login", gestioncof_views.LoginExtView.as_view(), name="ext_login_view"
    ),
    path(
        "outsider/logout", django_auth_views.LogoutView.as_view(), {"next_page": "home"}
    ),
    path("login", gestioncof_views.login, name="cof-login"),
    path("logout", gestioncof_views.logout, name="cof-logout"),
    # Infos persos
    path("profile", gestioncof_views.profile, name="profile"),
    path(
        "outsider/password-change",
        django_auth_views.PasswordChangeView.as_view(),
        name="password_change",
    ),
    path(
        "outsider/password-change-done",
        django_auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    # Inscription d'un nouveau membre
    path("registration", gestioncof_views.registration, name="registration"),
    path(
        "registration/clipper/<slug:login_clipper>/<fullname>",
        gestioncof_views.registration_form2,
        name="clipper-registration",
    ),
    path(
        "registration/user/<username>",
        gestioncof_views.registration_form2,
        name="user-registration",
    ),
    path(
        "registration/empty",
        gestioncof_views.registration_form2,
        name="empty-registration",
    ),
    # Autocompletion
    path(
        "autocomplete/registration", autocomplete, name="cof.registration.autocomplete"
    ),
    path(
        "user/autocomplete",
        gestioncof_views.user_autocomplete,
        name="cof-user-autocomplete",
    ),
    # Interface admin
    path("admin/logout/", gestioncof_views.logout),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path(
        "admin/<slug:app_label>/<slug:model_name>/csv/",
        csv_views.admin_list_export,
        {"fields": ["username"]},
    ),
    path("admin/", admin.site.urls),
    # Liens utiles du COF et du BdA
    path("utile_cof", gestioncof_views.utile_cof, name="utile_cof"),
    path("utile_bda", gestioncof_views.utile_bda, name="utile_bda"),
    path("utile_bda/bda_diff", gestioncof_views.liste_bdadiff, name="ml_diffbda"),
    path("utile_cof/diff_cof", gestioncof_views.liste_diffcof, name="ml_diffcof"),
    path(
        "utile_bda/bda_revente",
        gestioncof_views.liste_bdarevente,
        name="ml_bda_revente",
    ),
    path("k-fet/", include("kfet.urls")),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    # djconfig
    path("config", gestioncof_views.ConfigUpdate.as_view(), name="config.edit"),
    # js-reverse
    path("jsreverse/", urls_js, name="js_reverse"),
]

if "events" in settings.INSTALLED_APPS:
    # The new event application is still in development
    # → for now it is namespaced below events_v2
    # → when the old events system is out, move this above in the others apps
    urlpatterns += [path("event_v2/", include("events.urls"))]

if "debug_toolbar" in settings.INSTALLED_APPS:
    import debug_toolbar

    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]

if settings.DEBUG:
    # Si on est en production, MEDIA_ROOT est servi par Apache.
    # Il faut dire à Django de servir MEDIA_ROOT lui-même en développement.
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Wagtail for uncatched
urlpatterns += i18n_patterns(
    path("", include(wagtail_urls)), prefix_default_language=False
)
