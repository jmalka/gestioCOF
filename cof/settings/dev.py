"""
Django development settings for the cof project.
The settings that are not listed here are imported from .common
"""

import os

from .common import *  # NOQA
from .common import INSTALLED_APPS, MIDDLEWARE, TESTING

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

DEBUG = True

if TESTING:
    PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# As long as these apps are not ready for production, they are only available
# in development mode
INSTALLED_APPS += ["events", "bds", "clubs"]


# ---
# Apache static/media config
# ---

STATIC_URL = "/static/"
STATIC_ROOT = "/srv/gestiocof/static/"

MEDIA_ROOT = "/srv/gestiocof/media/"
MEDIA_URL = "/media/"


# ---
# Debug tool bar
# ---


def show_toolbar(request):
    """
    On ne veut pas la vérification de INTERNAL_IPS faite par la debug-toolbar
    car cela interfère avec l'utilisation de Vagrant. En effet, l'adresse de la
    machine physique n'est pas forcément connue, et peut difficilement être
    mise dans les INTERNAL_IPS.
    """
    env_no_ddt = bool(os.environ.get("DJANGO_NO_DDT", None))
    return DEBUG and not env_no_ddt and not request.path.startswith("/admin/")


if not TESTING:
    INSTALLED_APPS += ["debug_toolbar"]

    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE

    DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": show_toolbar}
