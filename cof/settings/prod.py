"""
Django development settings for the cof project.
The settings that are not listed here are imported from .common
"""

import os

from .common import *  # NOQA
from .common import BASE_DIR, INSTALLED_APPS, TESTING, import_secret

DEBUG = False

ALLOWED_HOSTS = ["cof.ens.fr", "www.cof.ens.fr", "dev.cof.ens.fr"]

if TESTING:
    INSTALLED_APPS += ["events", "clubs"]

STATIC_ROOT = os.path.join(
    os.path.dirname(os.path.dirname(BASE_DIR)), "public", "gestion", "static"
)

STATIC_URL = "/gestion/static/"
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), "media")
MEDIA_URL = "/gestion/media/"


RECAPTCHA_PUBLIC_KEY = import_secret("RECAPTCHA_PUBLIC_KEY")
RECAPTCHA_PRIVATE_KEY = import_secret("RECAPTCHA_PRIVATE_KEY")
