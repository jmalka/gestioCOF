import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from kfet.models import Account, Checkout

from .utils import create_user

User = get_user_model()


class AccountTests(TestCase):
    def setUp(self):
        self.account = Account(trigramme="000")
        self.account.save({"username": "user"})

    def test_password(self):
        self.account.change_pwd("anna")
        self.account.save()

        self.assertEqual(Account.objects.get_by_password("anna"), self.account)

        with self.assertRaises(Account.DoesNotExist):
            Account.objects.get_by_password(None)

        with self.assertRaises(Account.DoesNotExist):
            Account.objects.get_by_password("bernard")


class CheckoutTests(TestCase):
    def setUp(self):
        self.now = timezone.now()

        self.u = create_user()
        self.u_acc = self.u.profile.account_kfet

        self.c = Checkout(
            created_by=self.u_acc,
            valid_from=self.now,
            valid_to=self.now + datetime.timedelta(days=1),
        )

    def test_initial_statement(self):
        """A statement is added with initial balance on creation."""
        self.c.balance = 10
        self.c.save()

        st = self.c.statements.get()
        self.assertEqual(st.balance_new, 10)
        self.assertEqual(st.amount_taken, 0)
        self.assertEqual(st.amount_error, 0)

        # Saving again doesn't create a new statement.
        self.c.save()

        self.assertEqual(self.c.statements.count(), 1)
