from django.db import models
from django.utils.crypto import get_random_string


class GenericTeamTokenManager(models.Manager):
    def create_token(self):
        token = get_random_string(50)
        while self.filter(token=token).exists():
            token = get_random_string(50)
        return self.create(token=token)


class GenericTeamToken(models.Model):
    token = models.CharField(max_length=50, unique=True)

    objects = GenericTeamTokenManager()
