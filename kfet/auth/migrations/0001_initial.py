# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("auth", "0006_require_contenttypes_0002"),
        # Following dependency allows using Account model to set up the kfet
        # generic user in post_migrate receiver.
        ("kfet", "0058_delete_genericteamtoken"),
    ]

    operations = [
        migrations.CreateModel(
            name="GenericTeamToken",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                    ),
                ),
                ("token", models.CharField(unique=True, max_length=50)),
            ],
        )
    ]
