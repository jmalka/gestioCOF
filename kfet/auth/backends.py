from django.contrib.auth import get_user_model

from kfet.models import Account, GenericTeamToken

from .utils import get_kfet_generic_user

User = get_user_model()


class BaseKFetBackend:
    def get_user(self, user_id):
        """
        Add extra select related up to Account.
        """
        try:
            return User.objects.select_related("profile__account_kfet").get(pk=user_id)
        except User.DoesNotExist:
            return None


class AccountBackend(BaseKFetBackend):
    def authenticate(self, request, kfet_password=None):
        try:
            return Account.objects.get_by_password(kfet_password).user
        except Account.DoesNotExist:
            return None


class GenericBackend(BaseKFetBackend):
    def authenticate(self, request, kfet_token=None):
        try:
            team_token = GenericTeamToken.objects.get(token=kfet_token)
        except GenericTeamToken.DoesNotExist:
            return

        # No need to keep the token.
        team_token.delete()

        return get_kfet_generic_user()
