from django import forms
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.forms import widgets


class KFetPermissionsField(forms.ModelMultipleChoiceField):
    def __init__(self, *args, **kwargs):
        queryset = Permission.objects.filter(
            content_type__in=ContentType.objects.filter(app_label="kfet")
        )
        super().__init__(
            queryset=queryset, widget=widgets.CheckboxSelectMultiple, *args, **kwargs
        )

    def label_from_instance(self, obj):
        return obj.name
