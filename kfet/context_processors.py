from kfet.config import kfet_config


def config(request):
    return {"kfet_config": kfet_config}
