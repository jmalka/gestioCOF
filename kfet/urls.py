from django.contrib.auth.decorators import permission_required
from django.urls import include, path, register_converter

from kfet import autocomplete, converters, views
from kfet.decorators import teamkfet_required

register_converter(converters.TrigrammeConverter, "trigramme")

urlpatterns = [
    path("login/generic", views.login_generic, name="kfet.login.generic"),
    path("history", views.history, name="kfet.history"),
    # -----
    # Account urls
    # -----
    # Account - General
    path("accounts/", views.account, name="kfet.account"),
    path(
        "accounts/is_validandfree",
        views.account_is_validandfree_ajax,
        name="kfet.account.is_validandfree.ajax",
    ),
    # Account - Create
    path("accounts/new", views.account_create, name="kfet.account.create"),
    path(
        "accounts/new/user/<username>",
        views.account_create_ajax,
        name="kfet.account.create.fromuser",
    ),
    path(
        "accounts/new/clipper/<slug:login_clipper>/<fullname>",
        views.account_create_ajax,
        name="kfet.account.create.fromclipper",
    ),
    path(
        "accounts/new/empty",
        views.account_create_ajax,
        name="kfet.account.create.empty",
    ),
    path(
        "autocomplete/account_new",
        autocomplete.account_create,
        name="kfet.account.create.autocomplete",
    ),
    # Account - Search
    path(
        "autocomplete/account_search",
        autocomplete.account_search,
        name="kfet.account.search.autocomplete",
    ),
    # Account - Read
    path(
        "accounts/<trigramme:trigramme>", views.account_read, name="kfet.account.read"
    ),
    # Account - Update
    path(
        "accounts/<trigramme:trigramme>/edit",
        views.account_update,
        name="kfet.account.update",
    ),
    # Account - Delete
    path(
        "accounts/<trigramme:trigramme>/delete",
        views.AccountDelete.as_view(),
        name="kfet.account.delete",
    ),
    # Account - Groups
    path("accounts/groups", views.account_group, name="kfet.account.group"),
    path(
        "accounts/groups/new",
        permission_required("kfet.manage_perms")(views.AccountGroupCreate.as_view()),
        name="kfet.account.group.create",
    ),
    path(
        "accounts/groups/<int:pk>/edit",
        permission_required("kfet.manage_perms")(views.AccountGroupUpdate.as_view()),
        name="kfet.account.group.update",
    ),
    path(
        "accounts/negatives",
        permission_required("kfet.view_negs")(views.AccountNegativeList.as_view()),
        name="kfet.account.negative",
    ),
    # Account - Statistics
    path(
        "accounts/<trigramme:trigramme>/stat/operations/list",
        views.AccountStatOperationList.as_view(),
        name="kfet.account.stat.operation.list",
    ),
    path(
        "accounts/<trigramme:trigramme>/stat/operations",
        views.AccountStatOperation.as_view(),
        name="kfet.account.stat.operation",
    ),
    path(
        "accounts/<trigramme:trigramme>/stat/balance/list",
        views.AccountStatBalanceList.as_view(),
        name="kfet.account.stat.balance.list",
    ),
    path(
        "accounts/<trigramme:trigramme>/stat/balance",
        views.AccountStatBalance.as_view(),
        name="kfet.account.stat.balance",
    ),
    # -----
    # Checkout urls
    # -----
    # Checkout - General
    path(
        "checkouts/",
        teamkfet_required(views.CheckoutList.as_view()),
        name="kfet.checkout",
    ),
    # Checkout - Create
    path(
        "checkouts/new",
        teamkfet_required(views.CheckoutCreate.as_view()),
        name="kfet.checkout.create",
    ),
    # Checkout - Read
    path(
        "checkouts/<int:pk>",
        teamkfet_required(views.CheckoutRead.as_view()),
        name="kfet.checkout.read",
    ),
    # Checkout - Update
    path(
        "checkouts/<int:pk>/edit",
        teamkfet_required(views.CheckoutUpdate.as_view()),
        name="kfet.checkout.update",
    ),
    # -----
    # Checkout Statement urls
    # -----
    # Checkout Statement - General
    path(
        "checkouts/statements/",
        teamkfet_required(views.CheckoutStatementList.as_view()),
        name="kfet.checkoutstatement",
    ),
    # Checkout Statement - Create
    path(
        "checkouts/<int:pk_checkout>/statements/add",
        teamkfet_required(views.CheckoutStatementCreate.as_view()),
        name="kfet.checkoutstatement.create",
    ),
    # Checkout Statement - Update
    path(
        "checkouts/<int:pk_checkout>/statements/<int:pk>/edit",
        teamkfet_required(views.CheckoutStatementUpdate.as_view()),
        name="kfet.checkoutstatement.update",
    ),
    # -----
    # Article urls
    # -----
    # Category - General
    path(
        "categories/",
        teamkfet_required(views.CategoryList.as_view()),
        name="kfet.category",
    ),
    # Category - Update
    path(
        "categories/<int:pk>/edit",
        teamkfet_required(views.CategoryUpdate.as_view()),
        name="kfet.category.update",
    ),
    # Article - General
    path(
        "articles/", teamkfet_required(views.ArticleList.as_view()), name="kfet.article"
    ),
    # Article - Create
    path(
        "articles/new",
        teamkfet_required(views.ArticleCreate.as_view()),
        name="kfet.article.create",
    ),
    # Article - Read
    path(
        "articles/<int:pk>",
        teamkfet_required(views.ArticleRead.as_view()),
        name="kfet.article.read",
    ),
    # Article - Update
    path(
        "articles/<int:pk>/edit",
        teamkfet_required(views.ArticleUpdate.as_view()),
        name="kfet.article.update",
    ),
    # Article - Delete
    path(
        "articles/<int:pk>/delete",
        views.ArticleDelete.as_view(),
        name="kfet.article.delete",
    ),
    # Article - Statistics
    path(
        "articles/<int:pk>/stat/sales/list",
        views.ArticleStatSalesList.as_view(),
        name="kfet.article.stat.sales.list",
    ),
    path(
        "articles/<int:pk>/stat/sales",
        views.ArticleStatSales.as_view(),
        name="kfet.article.stat.sales",
    ),
    # -----
    # K-Psul urls
    # -----
    path("k-psul/", views.kpsul, name="kfet.kpsul"),
    path(
        "k-psul/checkout_data",
        views.kpsul_checkout_data,
        name="kfet.kpsul.checkout_data",
    ),
    path(
        "k-psul/perform_operations",
        views.kpsul_perform_operations,
        name="kfet.kpsul.perform_operations",
    ),
    path(
        "k-psul/cancel_operations",
        views.kpsul_cancel_operations,
        name="kfet.kpsul.cancel_operations",
    ),
    path(
        "k-psul/articles_data",
        views.kpsul_articles_data,
        name="kfet.kpsul.articles_data",
    ),
    path(
        "k-psul/update_addcost",
        views.kpsul_update_addcost,
        name="kfet.kpsul.update_addcost",
    ),
    path(
        "k-psul/get_settings", views.kpsul_get_settings, name="kfet.kpsul.get_settings"
    ),
    # -----
    # JSON urls
    # -----
    path("history.json", views.history_json, name="kfet.history.json"),
    path(
        "accounts/<trigramme:trigramme>/.json",
        views.account_read_json,
        name="kfet.account.read.json",
    ),
    # -----
    # Settings urls
    # -----
    path("settings/", views.config_list, name="kfet.settings"),
    path("settings/edit", views.config_update, name="kfet.settings.update"),
    # -----
    # Transfers urls
    # -----
    path("transfers/", views.transfers, name="kfet.transfers"),
    path("transfers/new", views.transfers_create, name="kfet.transfers.create"),
    path("transfers/perform", views.perform_transfers, name="kfet.transfers.perform"),
    path("transfers/cancel", views.cancel_transfers, name="kfet.transfers.cancel"),
    # -----
    # Inventories urls
    # -----
    path(
        "inventaires/",
        teamkfet_required(views.InventoryList.as_view()),
        name="kfet.inventory",
    ),
    path("inventaires/new", views.inventory_create, name="kfet.inventory.create"),
    path(
        "inventaires/<int:pk>",
        teamkfet_required(views.InventoryRead.as_view()),
        name="kfet.inventory.read",
    ),
    # -----
    # Order urls
    # -----
    path("orders/", teamkfet_required(views.OrderList.as_view()), name="kfet.order"),
    path(
        "orders/<int:pk>",
        teamkfet_required(views.OrderRead.as_view()),
        name="kfet.order.read",
    ),
    path(
        "orders/suppliers/<int:pk>/edit",
        teamkfet_required(views.SupplierUpdate.as_view()),
        name="kfet.order.supplier.update",
    ),
    path(
        "orders/suppliers/<int:pk>/new-order", views.order_create, name="kfet.order.new"
    ),
    path(
        "orders/<int:pk>/to_inventory",
        views.order_to_inventory,
        name="kfet.order.to_inventory",
    ),
]

urlpatterns += [
    # K-Fêt Open urls
    path("open/", include("kfet.open.urls"))
]
