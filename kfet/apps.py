from django.apps import AppConfig


class KFetConfig(AppConfig):
    name = "kfet"
    verbose_name = "Application K-Fêt"

    def ready(self):
        self.register_config()

    def register_config(self):
        import djconfig
        from kfet.forms import KFetConfigForm

        djconfig.register(KFetConfigForm)
