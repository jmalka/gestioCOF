# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.snippets.blocks
from django.db import migrations, models

import kfet.cms.models


class Migration(migrations.Migration):

    dependencies = [
        ("wagtailcore", "0033_remove_golive_expiry_help_text"),
        ("wagtailimages", "0019_delete_filter"),
    ]

    operations = [
        migrations.CreateModel(
            name="KFetPage",
            fields=[
                (
                    "page_ptr",
                    models.OneToOneField(
                        serialize=False,
                        primary_key=True,
                        parent_link=True,
                        auto_created=True,
                        to="wagtailcore.Page",
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    "no_header",
                    models.BooleanField(
                        verbose_name="Sans en-tête",
                        help_text="Coché, l'en-tête (avec le titre) de la page n'est pas affiché.",  # noqa
                        default=False,
                    ),
                ),
                (
                    "content",
                    wagtail.core.fields.StreamField(
                        (
                            (
                                "rich",
                                wagtail.core.blocks.RichTextBlock(label="Éditeur"),
                            ),
                            ("carte", kfet.cms.models.MenuBlock()),
                            (
                                "group_team",
                                wagtail.core.blocks.StructBlock(
                                    (
                                        (
                                            "show_only",
                                            wagtail.core.blocks.IntegerBlock(
                                                help_text="Nombre initial de membres affichés. Laisser vide pour tou-te-s les afficher.",  # noqa
                                                required=False,
                                                label="Montrer seulement",
                                            ),
                                        ),
                                        (
                                            "members",
                                            wagtail.core.blocks.ListBlock(
                                                wagtail.snippets.blocks.SnippetChooserBlock(  # noqa
                                                    kfet.cms.models.MemberTeam
                                                ),
                                                classname="team-group",
                                                label="K-Fêt-eux-ses",
                                            ),
                                        ),
                                    )
                                ),
                            ),
                            (
                                "group",
                                wagtail.core.blocks.StreamBlock(
                                    (
                                        (
                                            "rich",
                                            wagtail.core.blocks.RichTextBlock(
                                                label="Éditeur"
                                            ),
                                        ),
                                        ("carte", kfet.cms.models.MenuBlock()),
                                        (
                                            "group_team",
                                            wagtail.core.blocks.StructBlock(
                                                (
                                                    (
                                                        "show_only",
                                                        wagtail.core.blocks.IntegerBlock(  # noqa
                                                            help_text="Nombre initial de membres affichés. Laisser vide pour tou-te-s les afficher.",  # noqa
                                                            required=False,
                                                            label="Montrer seulement",
                                                        ),
                                                    ),
                                                    (
                                                        "members",
                                                        wagtail.core.blocks.ListBlock(
                                                            wagtail.snippets.blocks.SnippetChooserBlock(  # noqa
                                                                kfet.cms.models.MemberTeam  # noqa
                                                            ),
                                                            classname="team-group",
                                                            label="K-Fêt-eux-ses",
                                                        ),
                                                    ),
                                                )
                                            ),
                                        ),
                                    ),
                                    label="Contenu groupé",
                                ),
                            ),
                        ),
                        verbose_name="Contenu",
                    ),
                ),
                (
                    "layout",
                    models.CharField(
                        max_length=255,
                        choices=[
                            (
                                "kfet/base_col_1.html",
                                "Une colonne : centrée sur la page",
                            ),
                            (
                                "kfet/base_col_2.html",
                                "Deux colonnes : fixe à gauche, contenu à droite",
                            ),
                            (
                                "kfet/base_col_mult.html",
                                "Contenu scindé sur plusieurs colonnes",
                            ),
                        ],
                        help_text="Comment cette page devrait être affichée ?",
                        verbose_name="Template",
                        default="kfet/base_col_mult.html",
                    ),
                ),
                (
                    "main_size",
                    models.CharField(
                        max_length=255,
                        blank=True,
                        verbose_name="Taille de la colonne de contenu",
                    ),
                ),
                (
                    "col_count",
                    models.CharField(
                        max_length=255,
                        blank=True,
                        verbose_name="Nombre de colonnes",
                        help_text="S'applique au page dont le contenu est scindé sur plusieurs colonnes",  # noqa
                    ),
                ),
            ],
            options={
                "verbose_name": "page K-Fêt",
                "verbose_name_plural": "pages K-Fêt",
            },
            bases=("wagtailcore.page",),
        ),
        migrations.CreateModel(
            name="MemberTeam",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        auto_created=True,
                        serialize=False,
                        primary_key=True,
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        blank=True, max_length=255, verbose_name="Prénom", default=""
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        blank=True, max_length=255, verbose_name="Nom", default=""
                    ),
                ),
                (
                    "nick_name",
                    models.CharField(
                        verbose_name="Alias", blank=True, default="", max_length=255
                    ),
                ),
                (
                    "photo",
                    models.ForeignKey(
                        null=True,
                        related_name="+",
                        on_delete=django.db.models.deletion.SET_NULL,
                        verbose_name="Photo",
                        blank=True,
                        to="wagtailimages.Image",
                    ),
                ),
            ],
            options={"verbose_name": "K-Fêt-eux-se"},
        ),
    ]
