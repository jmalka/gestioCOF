var Account = Backbone.Model.extend({

    defaults: {
        'id': 0,
        'name': '',
        'email': '',
        'is_cof': '',
        'promo': '',
        'balance': '',
        'is_frozen': false,
        'departement': '',
        'nickname': '',
        'trigramme': '',
    },

    url: function () {
        return django_urls["kfet.account.read.json"](this.get("trigramme"))
    },

    reset: function () {
        // On ne veut pas trigger un `change` deux fois
        this.clear({ silent: true }).set(this.defaults)
    },

    parse: function (resp, options) {
        if (_.has(resp, 'balance')) {
            return _.extend(resp, { 'balance': parseFloat(resp.balance) });
        } else {
            return resp;
        }
    },

    view: function () {
        view_class = this.get("trigramme") == 'LIQ' ? LIQView : AccountView;
        return new view_class({ model: this })
    },

    render: function () {
        this.view().render();
    }
})

var AccountView = Backbone.View.extend({

    el: '#account',
    input: '#id_trigramme',
    buttons: '.buttons',

    props: _.keys(Account.prototype.defaults),

    get: function (property) {
        /* If the function this.get_<property> is defined,
        we call it ; else we call this.model.<property>. */
        getter_name = 'get_' + property;
        if (_.functions(this).includes(getter_name))
            return this[getter_name]()
        else
            return this.model.get(property)
    },

    get_is_cof: function () {
        return this.model.get("is_cof") ? 'COF' : 'Non-COF';
    },

    get_balance: function () {
        return amountToUKF(this.model.get("balance"), this.model.get("is_COF"), true)
    },

    attr_data_balance: function () {
        if (this.model.id == 0) {
            return '';
        } else if (this.model.get("balance") < 0) {
            return 'neg';
        } else if (this.model.get("balance") <= 5) {
            return 'low';
        } else {
            return 'ok';
        }
    },

    get_buttons: function () {
        var buttons = '';
        if (this.model.id != 0) {
            var url = django_urls["kfet.account.read"](this.model.get("trigramme"))
            buttons += `<a href="${url}" class="btn btn-primary" target="_blank" title="Modifier"><span class="glyphicon glyphicon-cog"></span></a>`;
        } else {
            var trigramme = this.$(this.input).val().toUpperCase();
            if (isValidTrigramme(trigramme)) {
                trigramme = encodeURIComponent(trigramme);
                var url_base = django_urls["kfet.account.create"]();
                var url = `${url_base}?trigramme=${trigramme}`;
                buttons += `<a href="${url}" class="btn btn-primary" target="_blank" title="Créer"><span class="glyphicon glyphicon-plus"></span></a>`;
            } else {
                buttons += '<button class="btn btn-primary search" title="Rechercher"><span class="glyphicon glyphicon-search"></span></button>';
            }
        }

        return buttons
    },

    render: function () {
        for (let prop of this.props) {
            var selector = "#account-" + prop;
            this.$(selector).text(this.get(prop));
        }

        this.$el.attr("data-balance", this.attr_data_balance());
        this.$(this.buttons).html(this.get_buttons());
    },

    reset: function () {
        for (let prop of this.props) {
            var selector = "#account-" + prop;
            this.$(selector).text('');
        }

        this.$el.attr("data-balance", '');
        this.$(this.buttons).html(this.get_buttons());
    },
})

var LIQView = AccountView.extend({
    get_balance: function () {
        return "";
    },

    attr_data_balance: function () {
        return 'ok';
    }
})

