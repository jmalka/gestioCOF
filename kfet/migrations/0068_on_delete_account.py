# Generated by Django 2.2 on 2019-05-23 16:17

from django.db import migrations, models

import kfet.models


class Migration(migrations.Migration):

    dependencies = [("kfet", "0067_deleted_account")]

    operations = [
        migrations.AlterField(
            model_name="checkout",
            name="created_by",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="checkoutstatement",
            name="by",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="inventory",
            name="by",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="operation",
            name="addcost_for",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="addcosts",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="operation",
            name="canceled_by",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="operationgroup",
            name="on_acc",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="opesgroup",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="operationgroup",
            name="valid_by",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="transfer",
            name="canceled_by",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="transfer",
            name="from_acc",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="transfers_from",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="transfer",
            name="to_acc",
            field=models.ForeignKey(
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="transfers_to",
                to="kfet.Account",
            ),
        ),
        migrations.AlterField(
            model_name="transfergroup",
            name="valid_by",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=models.SET(kfet.models.get_deleted_account),
                related_name="+",
                to="kfet.Account",
            ),
        ),
    ]
