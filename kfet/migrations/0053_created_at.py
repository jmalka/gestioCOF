# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("kfet", "0052_category_addcost")]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="created_at",
            field=models.DateTimeField(default=django.utils.timezone.now),
        )
    ]
