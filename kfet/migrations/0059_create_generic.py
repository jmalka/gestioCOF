# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

from kfet.auth import KFET_GENERIC_TRIGRAMME, KFET_GENERIC_USERNAME


def setup_kfet_generic_user(apps, schema_editor):
    """
    Setup models instances for the kfet generic account.

    Username and trigramme are retrieved from kfet.auth.__init__ module.
    Other data are registered here.

    See also setup_kfet_generic_user from kfet.auth.utils module.
    """
    User = apps.get_model("auth", "User")
    CofProfile = apps.get_model("gestioncof", "CofProfile")
    Account = apps.get_model("kfet", "Account")

    user, _ = User.objects.update_or_create(
        username=KFET_GENERIC_USERNAME,
        defaults={"first_name": "Compte générique K-Fêt"},
    )
    profile, _ = CofProfile.objects.update_or_create(user=user)
    account, _ = Account.objects.update_or_create(
        cofprofile=profile, defaults={"trigramme": KFET_GENERIC_TRIGRAMME}
    )


class Migration(migrations.Migration):

    dependencies = [("kfet", "0058_delete_genericteamtoken")]

    operations = [migrations.RunPython(setup_kfet_generic_user)]
