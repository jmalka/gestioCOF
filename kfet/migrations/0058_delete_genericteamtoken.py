# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("kfet", "0057_merge")]

    operations = [migrations.DeleteModel(name="GenericTeamToken")]
