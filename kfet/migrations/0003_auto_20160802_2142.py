# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("kfet", "0002_auto_20160802_2139")]

    operations = [
        migrations.AlterField(
            model_name="accountnegative",
            name="start",
            field=models.DateTimeField(default=datetime.datetime.now),
        )
    ]
