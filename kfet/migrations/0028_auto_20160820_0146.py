# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("kfet", "0027_auto_20160811_0648")]

    operations = [
        migrations.AlterField(
            model_name="operation",
            name="group",
            field=models.ForeignKey(
                to="kfet.OperationGroup",
                on_delete=django.db.models.deletion.PROTECT,
                related_name="opes",
            ),
        ),
        migrations.AlterField(
            model_name="operationgroup",
            name="checkout",
            field=models.ForeignKey(
                to="kfet.Checkout",
                on_delete=django.db.models.deletion.PROTECT,
                related_name="opesgroup",
            ),
        ),
        migrations.AlterField(
            model_name="operationgroup",
            name="on_acc",
            field=models.ForeignKey(
                to="kfet.Account",
                on_delete=django.db.models.deletion.PROTECT,
                related_name="opesgroup",
            ),
        ),
    ]
